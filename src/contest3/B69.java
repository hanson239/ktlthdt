package contest3;

import java.util.Scanner;

public class B69 {

    static final long mod = 1000000007;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int k = in.nextInt();

            long[] F = new long[n + 5];
            F[0] = 1;
            F[1] = 1;
            if (k == 1) F[2] = 1;
            else F[2] = 2;
            for (int i = 3; i <= n; i++) {
                for (int j = 1; j <= k; j++) {
                    if (i - j >= 0) {
                        F[i] =  F[i - j];
                        F[i] %= mod;
                    }
                }
            }
            System.out.println(F[n]);
        }
    }
}
