package contest3;

import java.util.Scanner;

public class B61 {

    static int[][] res;
    static boolean[][] checked;
    static String s1;
    static String s2;

    static int solve(int m, int n) {
        if (m == -1 || n == -1) return 0;
        if (checked[m][n]) return res[m][n];

        if (s1.charAt(m) == s2.charAt(n)) {
//            System.out.println(s1.charAt(m) + ": " + m + " - " + n);
            res[m][n] = solve(m - 1, n - 1) + 1;
        } else {
            res[m][n] = Math.max(solve(m - 1, n), solve(m, n - 1));
        }
        checked[m][n] = true;
        return res[m][n];
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            s1 = in.nextLine();
            s2 = in.nextLine();

            int lenS1 = s1.length();
            int lenS2 = s2.length();
            res = new int[lenS1][lenS2];
            checked = new boolean[lenS1][lenS2];

            System.out.println(solve(lenS1 - 1, lenS2 - 1));
        }
    }
}
