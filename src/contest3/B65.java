package contest3;

import java.util.Scanner;

public class B65 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while(test-- >0) {
            int n = in.nextInt();
            int[] arr = new int[n];
            int[] sum = new int[n];
            int res = 0;
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
                for (int j = 0; j < i; j++) {
                    if (arr[i] > arr[j]) {
                        sum[i] = Math.max(sum[i], sum[j]);
                    }
                }
                sum[i] += arr[i];
                res = Math.max(res, sum[i]);
            }
            System.out.println(res);
        }
    }
}
