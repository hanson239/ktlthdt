package contest3;

import java.util.Scanner;

public class B71 {

    static boolean isPalindrome(String s) {
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) return false;
            i++;
            j--;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            if (isPalindrome(s)) {
                System.out.println(0);
                continue;
            }
            
        }
    }
}
