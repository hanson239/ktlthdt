package contest3;

import java.util.Scanner;

public class B64 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        int[] count = new int[n];
        int res = 0;
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
            for (int j = i - 1; j >= 0; j--) {
                if (arr[i] > arr[j]) {
                    count[i] = Math.max(count[i], count[j]);
                }
            }
            count[i] += 1;
            res = Math.max(res, count[i]);
        }
        System.out.println(res);
    }
}
