package contest3;

import java.util.Scanner;

public class B63 {

    static int[][][] res;
    static boolean[][][] checked;
    static String s1;
    static String s2;
    static String s3;

    static int solve(int m, int n, int i) {
        if (m == -1 || n == -1 || i == -1) return 0;
        if (checked[m][n][i]) return res[m][n][i];

        if (s1.charAt(m) == s2.charAt(n) && s2.charAt(n) == s3.charAt(i)) {
            res[m][n][i] = solve(m - 1, n - 1, i - 1) + 1;
        } else {
            int temp = Math.max(solve(m - 1, n, i), solve(m, n - 1, i));
            temp = Math.max(temp, solve(m, n, i - 1));
            temp = Math.max(temp, solve(m - 1, n, i - 1));
            temp = Math.max(temp, solve(m, n - 1, i - 1));
            temp = Math.max(temp, solve(m - 1, n - 1, i));
            res[m][n][i] = temp;
        }
        checked[m][n][i] = true;
        return res[m][n][i];
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            int lenS1 = in.nextInt();
            int lenS2 = in.nextInt();
            int lenS3 = in.nextInt();
            in.nextLine();
            String s = in.nextLine();
            s = s.trim();
            String[] arr = s.split(" ");
            s1 = arr[0];
            s2 = arr[1];
            s3 = arr[2];
            res = new int[lenS1][lenS2][lenS3];
            checked = new boolean[lenS1][lenS2][lenS3];

            System.out.println(solve(lenS1 - 1, lenS2 - 1, lenS3 - 1));
        }
    }
}
