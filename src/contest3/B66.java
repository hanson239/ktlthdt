package contest3;

import java.util.Scanner;

public class B66 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
            }
            int ans = 0;
            int res[] = new int[n];
            for (int i = 0; i < n; i++) {
                res[i] = 1;
                for (int j = 0; j < i; j++) {
                    if (arr[j] <= arr[i]) {
                        res[i] = Math.max(res[i], res[j] + 1);
                    }
                }
                ans = Math.max(ans, res[i]);
            }
            System.out.println(n - ans);
        }
    }


}
