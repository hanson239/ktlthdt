package contest3;

import java.util.Scanner;

public class B67 {

    static void init(long[] res) {
        res[0] = 1;
        res[1] = 1;
        res[2] = 2;
        for (int i = 3; i <= 50; i++) {
            res[i] = res[i - 1] + res[i - 2] + res[i - 3];
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long[] res = new long[51];
        init(res);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            System.out.println(res[n]);
        }
    }
}
/*
1: 1
2: 11 2
3: 111 12 21 3
4: 1111 112 121 211 22 13 31
5: 11111 1112 1121 1211 2111 122 212 221 113 131 311 32 23

x + 2y + 3z = 4

*/
