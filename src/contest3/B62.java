package contest3;

import java.util.Scanner;

public class B62 {


    static int[][] res;
    static boolean[][] checked;
    static String s;

    static int solve(int m, int n) {

        if (m == -1 || n == -1) return 0;
        if (checked[m][n]) return res[m][n];

        if (m != n && s.charAt(m) == s.charAt(n)) {
            res[m][n] = solve(m - 1, n - 1) + 1;
        } else {
            res[m][n] = Math.max(solve(m - 1, n), solve(m, n - 1));
        }
        checked[m][n] = true;
        return res[m][n];

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int len = in.nextInt();
            in.nextLine();
            s = in.nextLine();

            res = new int[len][len];
            checked = new boolean[len][len];

            System.out.println(solve(len - 1, len - 1));
        }
    }


}
