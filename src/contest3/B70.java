package contest3;

import java.util.Scanner;

public class B70 {

    static int solve(String s) {
        int n = s.length();
        s = " " + s;
        boolean f[][] = new boolean[n + 5][n + 5];

        for (int i = 0; i <= n; i++) {
            f[i][i] = true;
        }

        for (int j = 1; j <= n; j++) { // Chu y for j  for i
            for (int i = 1; i < j; i++) {
                if (s.charAt(i) == s.charAt(j) && i != j - 1) {
                    f[i][j] = f[i + 1][j - 1];
                } else if (s.charAt(i) == s.charAt(j) && i == j - 1) {
                    f[i][j] = true;
                } else {
                    f[i][j] = false;
                }
            }
        }
        int ans = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (f[i][j] == true) {
                    ans = Math.max(ans, j - i + 1);
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            System.out.println(solve(s));
        }
    }
}
