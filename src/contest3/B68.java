package contest3;

import java.util.Scanner;

public class B68 {

    static final int mod = 1000000007;


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        int C[][] = new int[1005][1005];
        for (int i = 0; i <= 1000; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    C[i][j] = 1;
                } else {
                    C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % mod;
                }
            }
        }
        while (test-- > 0) {
            int n = in.nextInt();
            int k = in.nextInt();
            System.out.println(C[n][k]);
        }
    }
}
