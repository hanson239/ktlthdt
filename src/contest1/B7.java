package contest1;

import java.util.Scanner;
import java.util.Stack;

public class B7 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        char[] arr = s.toCharArray();
        int n = s.length()-1;
        Stack<Character> st = new Stack<Character>();
        for(char c : arr){
            if (st.empty()){
                st.push(c);
                continue;
            }
            char c1 = st.pop();
            if (c1 != c){
                st.push(c1);
                st.push(c);
            }
        }
        if (st.empty()){
            System.out.println("Empty String");
        }
        else
        for (char c: st){
            System.out.print(c);
        }

    }
}
