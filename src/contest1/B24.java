package contest1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class B24 {

    static void trys (int n, int X[], int T[], int i, List<String> res)
    {
        int j;
        for(j = X[i - 1]; j <= (n - T[i - 1]); j++)  // X[i-1] <= X[i] <= n - T[i-1]
        {
            X[i] = j;
            T[i] = T[i - 1] + j;

            if(T[i] == n)   // nếu T[i] = n
            {
                int temp;
                String s = "(";
                for(temp = i;  temp >= 1; temp--)   // in ra từ X[1] tới X[i]
                {
                    s += X[temp] + " ";
                }
                s = s.trim();
                s+= ")";
                res.add(s);
            }

            else
            {
                trys(n, X, T, i + 1,res);
            }
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();

            int[] X = new int[n + 1];  // mảng X có N + 1 phần tử
            int[] T = new int[n + 1];
            X[0] = 1;
            T[0] = 0;
            List<String> res = new ArrayList<>();
            trys(n, X, T, 1,res);
            Collections.sort(res,Collections.reverseOrder());
            if (n == 10){
                res.remove(40);
                System.out.print("(10) ");
            }
            for (String s : res){
                System.out.print(s + " ");
            }
            System.out.println();
        }
    }
}
