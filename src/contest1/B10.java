package contest1;

import java.util.*;

public class B10 {

    static void swap (int[] arr, int indexLeft, int indexRight){
        int temp = arr[indexLeft];
        arr[indexLeft] = arr[indexRight];
        arr[indexRight] = temp;
    }

    static void reverse (int[] arr, int indexLeft, int indexRight){
        while (indexLeft < indexRight){
            swap(arr,indexLeft,indexRight);
            indexLeft++;
            indexRight--;
        }
    }

    static int[] next_permutation(int[] arr, int n){
        int i = 0;
        for ( i = n-1; i > 1; i--){
            if ( arr[i] > arr[i-1] ) break;
        }
        i--;
        int j = 0;
        for ( j = n-1; j > 1; j--){
            if ( arr[j] > arr[i] ) break;
        }
        swap(arr,i,j);
        reverse(arr,i+1, n-1);
        return arr;
    }

    static int factorialOfN(int n){
        if (n==1) return 1;
        return n * factorialOfN(n-1);
    }

    static List<String> allPermutationOfN(int n){
        int factorial = factorialOfN(n);
        int[] arr = new int[n];
        List<String> res = new ArrayList<>();
        for (int i = 0; i < n; i++){
            arr[i] = i+1;
        }
        while(true){
            String s = "";
            for (int c : arr){
                s+=c;
            }
            res.add(s);
            if (res.size() == factorial) return res;
            arr = next_permutation(arr,n);
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            List<String> result = allPermutationOfN(n);
            for (int i = result.size()-1 ; i >= 0; i--){
                System.out.print( result.get(i) + " ");
            }
            System.out.println();
        }
    }
}
