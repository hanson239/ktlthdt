package contest1;

import java.util.PriorityQueue;
import java.util.Scanner;


public class B14 {

    public static void main(String[] args) {
        long mod = 1000000007;
        Scanner in = new Scanner(System.in);
        long test = in.nextLong();
        while (test-- > 0){
            long n = in.nextLong();
            long res = 0;
            PriorityQueue<Long> queue = new PriorityQueue<>();
            for (long i = 1; i <= n; i++){
                queue.add(in.nextLong());
            }
            while (queue.size() > 1){
                long temp1 = queue.poll();
                long temp2 = queue.poll();
                temp1 = (temp1 + temp2) % mod;
                res = (res + temp1) % mod;
                queue.add(temp1);
            }

            System.out.println(res);
        }
    }
}

