package contest1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class B9 {

    static void swap (int[] arr, int indexLeft, int indexRight){
        int temp = arr[indexLeft];
        arr[indexLeft] = arr[indexRight];
        arr[indexRight] = temp;
    }

    static void reverse (int[] arr, int indexLeft, int indexRight){
        while (indexLeft < indexRight){
            swap(arr,indexLeft,indexRight);
            indexLeft++;
            indexRight--;
        }
    }
    static int[] next_permutation(int[] arr, int n){
        int i = 0;
        for ( i = n; i > 1; i--){
            if ( arr[i] > arr[i-1] ) break;
        }
        i--;
        if (i < 1) {
            Arrays.sort(arr, 0, n+1);
            return arr;
        }
        int j = 0;
        for ( j = n; j > 1; j--){
            if ( arr[j] > arr[i] ) break;
        }
        swap(arr,i,j);
        reverse(arr,i+1, n);
        return arr;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            int[] arr = new int[1001];
            for (int i = 1; i <= n; i++){
                arr[i] = in.nextInt();
            }
            int[] res = next_permutation(arr,n);
            for (int i = 1; i <= n ; i++) {
                System.out.print(res[i] + " ");
            }
            System.out.println();
        }
    }
}
