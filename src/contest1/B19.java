package contest1;

import java.util.Arrays;
import java.util.Scanner;

public class B19 {

    static int[] nextBinary (int[] arr,int n){
        for (int i = n - 1; i >= 0; i--){
            if (arr[i] == 0){
                arr[i] = 1;
                for (int j = i+1; j<n; j++){
                    arr[j] = 0;
                }
                return arr;
            }
        }
        return arr;
    }

    static boolean check(String s){
        int root3 = (int)Math.cbrt(Long.parseLong(s));
        if (root3 * root3 * root3 == Long.parseLong(s)){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String number = in.nextLine();
            if (check(number)){
                System.out.println(number);
                continue;
            }
            int[] arr = new int[number.length()];

            Arrays.fill(arr,0);

            long res = Long.MIN_VALUE;

            while (true){
                String temp = "";
                arr = nextBinary(arr, number.length());
                for (int i = 0;i<number.length();i++){
                    if (arr[i] == 0){
                        temp += String.valueOf(number.charAt(i));
                    }
                }
                if (temp.length() == 0) break;
                if (check(temp)){
//                    if (res != Long.MIN_VALUE){
//                        int l = String.valueOf(res).length();
//                        if (l > temp.length()) break;
//                    }
                    res = Math.max(res,Long.parseLong(temp));
                }
            }
            if (res == Long.MIN_VALUE) System.out.println(-1);
            else{
                System.out.println(res);
            }
        }
    }
}
