package contest1;

import java.util.*;
public class B30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        Stack<Character> stack=new Stack<>();
        while (t-- > 0) {
            String s=sc.nextLine();
            int xoa=0, res=0;
            for(int i=0;i< s.length();i++){
                if(stack.size()==0){
                    stack.push(s.charAt(i));
                }
                else if(stack.peek()=='[' && s.charAt(i)==']'){
                    stack.pop();
                    xoa+=2;
                }
                else if(stack.peek()==']' && s.charAt(i)=='['){
                    res += stack.size()+xoa;
                    stack.pop();
                }
                else if(s.charAt(i)=='['){
                    stack.push(s.charAt(i));
                }
                else if(stack.peek()==']' && s.charAt(i)==']'){
                    stack.push(s.charAt(i));
                }
                if(stack.size()==0) xoa =0;
            }
            System.out.println(res);
        }
    }
}
