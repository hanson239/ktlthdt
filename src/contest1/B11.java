package contest1;

import java.util.*;
import java.lang.*;
import java.util.stream.Collectors;

class B11 {
    static int[] nextBinary (int[] arr, int length){
        for (int i = length - 1; i >= 0; i--){
            if (arr[i] == 0){
                arr[i] = 1;
                for (int j = i+1; j<length; j++){
                    arr[j] = 0;
                }
                return arr;
            }
        }
        return arr;
    }
    public static void main(String[] args) {
        Scanner cin = new Scanner (System.in);
        int test = cin.nextInt();
        while (test-- > 0){
            int n = cin.nextInt();
            cin.nextLine();
            String s = cin.nextLine();
            char[] arr = s.toCharArray();
            int[] binary = new int[n];
            Arrays.fill(binary,0);
            Arrays.sort(arr);
            List<String> res = new ArrayList<>();
            int dem = (int)Math.pow(2,n) - 1;
            while (dem-- > 0){
                binary = nextBinary(binary, n);
                String s2 = "";
                for (int i = 0;i<n;i++){
                    if (binary[i] == 1){
                        s2 += String.valueOf(arr[i]);
                    }
                }
                res.add(s2);
            }
            Collections.sort(res);
            for (String ss : res) {
                System.out.print(ss + " ");
            }
            System.out.println();
        }


    }
}
