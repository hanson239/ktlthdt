package contest1;

import java.util.Scanner;

public class B22 {
    static int max = 0;
    static boolean Ok(int[] a,int x2,int y2){
        for(int i = 1; i < x2 ;i++)
            if(a[i] == y2 || Math.abs(i-x2) == Math.abs(a[i] - y2) )
                return false;
        return true;
    }

    static void Max(int[] arr, int[][] point){
        int score = 0;
        for (int i = 1; i<= 8; i++){
            score += point[i][arr[i]];
        }
        if (score > max) max = score;
    }
    static void Try(int[] a, int[][] point, int i,int n){
        for(int j = 1;j<=n;j++){
            if(Ok(a,i,j)){
                a[i] = j;
                if(i==n) Max(a,point);
                Try(a,point,i+1,n);
            }
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int[][] point = new int[9][9];
            for (int i = 1; i<=8; i++){
                for (int j = 1; j<=8 ;j++)
                    point[i][j] = in.nextInt();
            }
            max = 0;
            int[] arr = new int[9];
            Try(arr,point,1,8);
            System.out.println(max);
        }
    }
}
