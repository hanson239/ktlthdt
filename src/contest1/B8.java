package contest1;

import java.util.Scanner;

public class B8 {

    static String solve (String s){
        char arr[] = s.toCharArray();
        for (int i = s.length() - 1; i >= 0; i--){
            if (arr[i] == '0'){
                arr[i] = '1';
                for (int j = i+1; j<s.length(); j++){
                    arr[j] = '0';
                }
                return String.valueOf(arr);
            }
        }
        for(int i = s.length() - 1; i >= 0; i--){
            arr[i] = '0';
        }
        return String.valueOf(arr);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String s = in.nextLine();
            System.out.println(solve(s));
        }
    }
}
