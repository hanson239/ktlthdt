package contest1;

import java.util.Scanner;

public class B27 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        String sa = String.valueOf(a);
        String sb = String.valueOf(b);

        sa = sa.replaceAll("5","6");
        sb = sb.replaceAll("5","6");
        int max = Integer.parseInt(sa) + Integer.parseInt(sb);

        sa = sa.replaceAll("6","5");
        sb = sb.replaceAll("6","5");
        int min = Integer.parseInt(sa) + Integer.parseInt(sb);

        System.out.println(min + " " + max);
    }
}
