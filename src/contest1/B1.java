package contest1;

import java.util.Scanner;

public class B1 {
    static long[] f = new long[93];
    static void init(){
        f[0]=0;
        f[1] = 1;
        f[2] = 1;
        for (int i = 3; i<=92; i++){
            f[i] = f[i-1] + f[i-2];
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        init();
        int test =  in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            System.out.println(f[n]);
        }
    }
}
