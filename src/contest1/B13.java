package contest1;

import java.util.*;

public class B13 {

    public static void quickSort(int[] arr,int[] arr2, int left, int right) {
        if (arr == null || arr.length == 0)
            return;

        if (left >= right)
            return;

        int middle = left + (right - left) / 2;
        int pivot = arr[middle];
        int i = left, j = right;

        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }

            while (arr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;

                temp = arr2[i];
                arr2[i] = arr2[j];
                arr2[j] = temp;
                i++;
                j--;
            }
        }

        if (left < j)
            quickSort(arr,arr2, left, j);

        if (right > i)
            quickSort(arr,arr2, i, right);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            int[] start = new int[n];
            int[] end = new int[n];
            for (int i = 0; i<n ;i++) {
                start[i] = in.nextInt();
                end[i] = in.nextInt();
            }

            quickSort(end,start,0,n-1);

            int count=0;
            int x=start[0];
            for (int i=0; i<n; i++)
            {
                if (x<=start[i])
                {
                    count++;
                    x=end[i];
                }
            }
            System.out.println(count);
        }
    }
}
