package contest1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class B3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String s = in.nextLine();
            char[] arr = s.toCharArray();
            int number = 0;
            Arrays.sort(arr);
            int index = 0;
            while ( index < s.length() && arr[index] <='9' && arr[index] >= '0' ){
                number += arr[index] - '0';
                index ++;
            }
            while(index < s.length()){
                System.out.print(arr[index]);
                index ++;
            }
            System.out.println(number);
        }
    }
}
