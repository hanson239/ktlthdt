package contest1;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class B6 {

    static String solve(String s){
        String res = "";
        s = s.toLowerCase().trim();
        String[] arr = s.split("\\s{1,}");
        res += arr[arr.length - 1];
        for (int i=0; i<arr.length-1;i++){
            res += arr[i].subSequence(0,1);
        }
        return res.trim();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        Set<String> res = new HashSet();
        while (test-- > 0){
            String s = in.nextLine();
            String resSolve = solve(s);
            String check = resSolve + "@ptit.edu.vn";
            if (!res.contains(check)) {
                res.add(check);
                System.out.println(check);
            }
            else {
                int index = 2;
                check = resSolve + String.valueOf(index) + "@ptit.edu.vn";
                while (res.contains(check)){
                    index++;
                    check = resSolve + String.valueOf(index) + "@ptit.edu.vn";
                }
                res.add(check);
                System.out.println(check);
            }
        }
    }
}
