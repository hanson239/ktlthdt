package contest1;

import java.util.Scanner;

public class B5 {

    static String solve(String s){
        String res = "";

        s = s.toLowerCase().trim();
        String[] arr = s.split("\\s{1,}");
        for (String ss : arr) {
            char[] c = ss.toCharArray();
            c[0] -= 32;
            ss = String.valueOf(c);
            res += ss + " ";
        }
        return res.trim();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String s = in.nextLine();
            System.out.println(solve(s));
        }
    }
}
