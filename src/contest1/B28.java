package contest1;

import java.util.Arrays;
import java.util.Scanner;

public class B28 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i< n; i++){
                arr[i] = in.nextInt();
            }
            Arrays.sort(arr);
            long n1 = 0;
            long n2 = 0;

            for (int i = 0; i < n; i++) {
                if ( arr[i] == 0 ) continue;
                if (i % 2 == 0) n1 = n1 * 10 + arr[i];
                else n2 = n2*10 + arr[i];
            }
            System.out.println(n1 + n2);
        }
    }
}
