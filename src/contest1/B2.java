package contest1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class B2 {
    static boolean isPrime(long n) {
        if (n <= 1)
            return false;
        if (n%2==0) return false;
        for (long i = 3; i <= Math.sqrt(n); i+=2)
            if (n % i == 0)
                return false;
        return true;
    }

    static List<Long> primeNumber = new ArrayList<>();

    static void init(){
        primeNumber.add((long)2);
        for (long i = 3; i<=99991; i+=2){
            if (isPrime(i)){
                primeNumber.add(i);
            }
        }
    }
    static long solve(long n){
        int i = 0;
        while (i < primeNumber.size()){
            while (n % primeNumber.get(i) == 0){
                if (n/primeNumber.get(i) == 1){
                    return n;
                }
                n /= primeNumber.get(i);
            }
            i++;
        }
        return n;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        init();
        int test = in.nextInt();
        while (test-- > 0) {
            long n = in.nextLong();
            System.out.println(solve(n));
        }
    }
}
