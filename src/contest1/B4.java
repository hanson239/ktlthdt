package contest1;

import java.util.Scanner;

public class B4 {

    static String solve(String s){
        int n = s.length() - 1;
        int i = 0;
        while (i< (n+1)/2){
            if (s.charAt(i) != s.charAt(n-i)) return "NO";
            if ((s.charAt(i) - '0') % 2 != 0) return "NO";
            i++;
        }
        return "YES";
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String s = in.nextLine();
            System.out.println(solve(s));
        }
    }
}
