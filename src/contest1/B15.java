package contest1;

import java.util.*;

public class B15 {

    static void solve(int x, int y, Stack s, int[][] map, int n, List res ){
        if (y + 1 < n && map[x][y+1] == 1 ){
            s.push("R");
            solve(x,y+1,s,map,n,res);
        }
        if (x + 1 < n && map[x+1][y] == 1){
            s.push("D");
            solve(x+1,y,s,map,n,res);
        }
        if (x == n - 1 && y == n - 1){
            String ss = "";
            String sres = "";
            ss = String.valueOf(s);
            for(int i = 0; i<ss.length(); i++){
                if (ss.charAt(i) =='D' || ss.charAt(i) =='R')  sres += String.valueOf(ss.charAt(i));
            }
            res.add(sres);
            s.pop();
        }else
        if (!s.empty()) s.pop();

    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            int[][] map = new int[n][n];
            int[][] ok = new int[n][n];
            for (int i = 0; i < n ; i++){
                for (int j = 0; j < n ; j++) {
                    map[i][j] = in.nextInt();
                    ok[i][j] = 0;
                }
            }
            if (map[0][0] == 0 || map[n-1][n-1] == 0){
                System.out.println("-1");
                continue;
            }
            Stack<String> st = new Stack<>();
            List<String> res = new ArrayList<>();
            solve(0,0,st,map,n,res);
            if (res.size() == 0) {
                System.out.println("-1");
                continue;
            }
            Collections.sort(res);
            for (String s : res) System.out.print(s + " ");
            System.out.println();
        }
    }
}
