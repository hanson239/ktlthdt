package contest1;

import java.util.*;

public class B18 {

    static void Try(int n, int X[],int Sum, int i, List<String> res,Stack st) {

        Sum += X[i];
        st.push(X[i]);
        if (Sum == n){
            String s = "[";
            Object[] arr = st.toArray();
            for (Object o : arr) {
                s += o.toString() + " ";
            }
            s = s.trim();
            s += "]";
            res.add(s);
            st.pop();
            return;
        }
        for (int j = i; j<X.length;j++){
            if (Sum + X[j] <= n)
                Try(n,X,Sum,j,res,st);
        }
        st.pop();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int k = in.nextInt();
            int n = in.nextInt();
            List<String> res = new ArrayList<>();
            int[] X = new int[k + 1];
            for (int i = 1; i <= k; i++) {
                X[i] = in.nextInt();
            }
            X[0]=0;
            Arrays.sort(X);
            if (X[1] > n) {
                System.out.println(-1);
                continue;
            }
            for (int i = 1 ; i <= k ; i++) {
                Stack<Integer> st = new Stack<>();
                Try(n, X, 0, i, res,st);
            }
            if (res.size() == 0) {
                System.out.println(-1);
                continue;
            }
            for (String s : res) {
                System.out.print(s);
            }
            System.out.println();
        }
    }
}
