package contest1;

import java.util.Arrays;
import java.util.Scanner;

public class B25 {
    static void swap (int[] arr, int indexLeft, int indexRight){
        int temp = arr[indexLeft];
        arr[indexLeft] = arr[indexRight];
        arr[indexRight] = temp;
    }

    static void reverse (int[] arr, int indexLeft, int indexRight){
        while (indexLeft < indexRight){
            swap(arr,indexLeft,indexRight);
            indexLeft++;
            indexRight--;
        }
    }

    static int[] next_permutation(int[] arr, int n){
        int i = 0;
        for ( i = n-1; i > 1; i--){
            if ( arr[i] > arr[i-1] ) break;
        }
        i--;
        int j = 0;
        for ( j = n-1; j > 1; j--){
            if ( arr[j] > arr[i] ) break;
        }
        swap(arr,i,j);
        reverse(arr,i+1, n-1);
        return arr;
    }

    static int factorialOfN(int n){
        if (n==1) return 1;
        return n * factorialOfN(n-1);
    }

    static void allPermutationOfN(char[] arrChar, int n){
        int factorial = factorialOfN(n);
        int[] arr = new int[n];
        for (int i = 0; i < n; i++){
            arr[i] = i;
        }
        int count = 1;
        while(true){
            for (int i : arr){
                System.out.print(arrChar[i]);
            }
            System.out.print(" ");
            if (count++ >= factorial) return;
            arr = next_permutation(arr,n);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String s = in.nextLine();
            char[] arr = s.toCharArray();
            Arrays.sort(arr);
            int n = s.length();
            allPermutationOfN(arr,n);
            System.out.println();
        }
    }
}
