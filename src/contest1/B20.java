package contest1;

import java.util.*;

public class B20 {

    static long UCLN(long a, long b){
        if(b == 0) return a;
        if(a == 0) return b;
        return UCLN(b, a % b);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            long a = in.nextLong();
            long b = in.nextLong();

            long gcd = UCLN(a,b);
            a /= gcd;
            b /= gcd;
            List<Long> res = new ArrayList<>();

            while ( a!=1){
                long x = (b/a) + 1;
                a = a * x - b;
                b = b * x;
                if (a < 0 || b < 0) {
                    System.out.println(a + " " + b);
                    break;
                }
                gcd = UCLN(a,b);
                a /= gcd;
                b /= gcd;
                res.add(x);

            }
            if (b>0)
                res.add(b);
            for (int i = 0 ; i<res.size()-1;i++) {
                System.out.print("1/" + res.get(i) + " + ");
            }
            System.out.println("1/" + res.get(res.size()-1));
        }
    }
}
