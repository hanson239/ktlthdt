package contest1;

import java.util.Arrays;
import java.util.Scanner;

public class B12 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            int k = in.nextInt();
            int[] count = new int[40];
            Arrays.fill(count,0);

            int[] arr = new int[k];

            for (int i = 0; i < k; i++ ){
                arr[i] = in.nextInt();
                count[arr[i]] ++;
            }


            for (int i = k - 1 ; i >= 0 ; i--){
                if (arr[i] < n - (k-i-1)){ // 5 - (3 - 1 - 1)
                    arr[i] ++;
                    count[arr[i]] += 2;
                    for (int j = i + 1; j < k ; j++){
                        arr[j] = arr[j-1] + 1;
                        count[arr[j]] += 2;
                    }
                    break;
                }
            }

            int res = 0;
            for (int i : count) {
                if (i == 2) res ++;
            }
            if (res == 0) res = k;
            System.out.println(res);
        }
    }
}
