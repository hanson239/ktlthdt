package contest1;

import java.util.Scanner;

public class B23 {
    static long S;
    static int n;
    static int[] arr;
    static long res = Long.MAX_VALUE;
    static void Try(int i , long sum , int d){
        if (sum>S || d>res) return;
        if(i==n){
            if (sum == S) res = Math.min(res,d);
            return;
        }
        Try(i+1,sum,d);
        Try(i+1,sum+arr[i],d+1);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            res = Long.MAX_VALUE;
            n = in.nextInt();
            S = in.nextLong();
            arr = new int[n];
            for (int i = 0; i<n;i++){
                arr[i] = in.nextInt();
            }
            Try(0,0,0);
            if (res == Long.MAX_VALUE) System.out.println(-1);
            else System.out.println(res);
        }
    }
}
