package contest1;

import java.util.Scanner;

public class B17 {

    static int indexOfMax(char[] arr, int length, int startIndex){
        int index = startIndex;
        char max = arr[startIndex];
        for(int i = length - 1; i >= startIndex + 1; i--){
            if (max < arr[i]){
                max = arr[i];
                index = i;
            }
        }
        return index;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int count = in.nextInt();
            in.nextLine();
            String number = in.nextLine();
            char[] arr = number.toCharArray();
            int startIndex = 0;
            int length = number.length();
            while (count > 0 && startIndex < length){
                int indexMax = indexOfMax(arr,length,startIndex);
                if (indexMax == startIndex){
                    startIndex ++;
                    continue;
                }
                char temp = arr[startIndex];
                arr[startIndex] = arr[indexMax];
                arr[indexMax] = temp;
                count--;
                startIndex++;
            }
            System.out.println(String.valueOf(arr));
        }
    }
}
