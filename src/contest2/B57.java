package contest2;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class B57 {

    static boolean isPrime[] = new boolean[10001];

    static void init() {
        Arrays.fill(isPrime, true);
        isPrime[1] = false;
        for (int i = 2; i <= 100; ++i)
            if (isPrime[i])
                for (int j = 2; i * j <= 10000; isPrime[i * j++] = false) ;
    }

    static int solve(int s, int t) {
        Queue<Integer> q = new LinkedList<>();
        if (s == t) return 0;
        q.add(s);
        int[] step = new int[10001];
        step[s] = 1;
        while (!q.isEmpty()) {
            int number = q.poll();
            for (int i = 3; i >=0; i--)
                for (int j = 0; j < 10; j++) {
                    StringBuilder ss = new StringBuilder(String.valueOf(number));
                    ss.setCharAt(i,(char)(j+'0'));
                    int temp = Integer.parseInt(ss.toString());
                    if (temp > 1000 && isPrime[temp] && step[temp] == 0) {
                        q.add(temp);
                        step[temp] = step[number] + 1;
                        if (temp == t) return (step[t] - 1);
                    }
                }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        init();
        int test = in.nextInt();
        while (test-- > 0) {
            int s = in.nextInt();
            int t = in.nextInt();
            System.out.println(solve(s, t));
        }
    }
}
