package contest2;

import java.util.Scanner;

public class B32 {


    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void sout(int count, int[] arr) {
        System.out.print("Buoc " + count + ":");
        for (int i : arr) {
            System.out.print( " " + i);
        }
        System.out.println();
    }

    static int indexOfMin(int[] arr, int length, int startIndex){
        int index = startIndex;
        int min = arr[startIndex];
        for(int i = startIndex + 1; i < length; i++){
            if (min > arr[i]){
                min = arr[i];
                index = i;
            }
        }
        return index;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        int startIndex = 0;

        while (startIndex < n-1){
            int indexMin = indexOfMin(arr,n,startIndex);
            if (indexMin == startIndex){
                sout(startIndex+1,arr);
                startIndex ++;

                continue;
            }
            swap(arr,startIndex,indexMin);
            sout(startIndex+1,arr);
            startIndex++;
        }

    }
}
