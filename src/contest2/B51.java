package contest2;

import java.util.Scanner;

public class B51 {

    static String[] res = new String[10001];

    static void init(){
        for (int i = 1; i <= 10000;i++){
            res[i] = Integer.toBinaryString(i);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        init();
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            for (int i = 1; i<=n;i++){
                System.out.print(res[i] + " ");
            }
            System.out.println();
        }
    }
}
