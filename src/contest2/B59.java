package contest2;

import java.util.*;

public class B59 {

    static int[] a = new int[6];
    static int[] b = new int[6];

    static int BFS() {
        Queue<int[]> qArr = new LinkedList<>();
        Queue<Integer> q = new LinkedList<>();
        Set<int[]> st = new HashSet<>();
        qArr.add(a);
        q.add(0);
        st.add(a);
        while (!q.isEmpty()) {
            int[] arr = qArr.poll();
            int res = q.poll();
            if (arr == b) {
                return res;
            }
            int[] c = Arrays.copyOf(arr, 6);
            c[0] = arr[3];
            c[3] = arr[4];
            c[4] = arr[1];
            c[1] = arr[0];
            int[] d = Arrays.copyOf(arr, 6);
            d[1] = arr[4];
            d[4] = arr[5];
            d[5] = arr[2];
            d[2] = arr[1];
            if (Arrays.equals(c, b) || Arrays.equals(d, b))
                return res + 1;
            if (!st.contains(c)) {
                st.add(c);
                qArr.add(c);
                q.add(res + 1);
            }
            if (!st.contains(d)) {
                qArr.add(d);
                q.add(res + 1);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            for (int i = 0; i < 6; i++) a[i] = in.nextInt();
            for (int i = 0; i < 6; i++) b[i] = in.nextInt();

            System.out.println(BFS());
        }
    }
}
