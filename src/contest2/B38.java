package contest2;

import java.util.Scanner;

public class B38 {

    static final int module = 1000000007;

    static long[][] multi_Matrix(long[][] a, long[][] b){
        long[][] res = new long[2][2];
        for (int i=0; i<=1; i++)
            for (int j=0; j<=1; j++)
            {
                res[i][j] = 0;
                for (int k=0; k<=1; k++)
                    res[i][j] = (res[i][j]+a[i][k]*b[k][j])%module;
            }
        return res;
    }

    static long[][] power_Matrix(long[][] a, int n){
        if (n==1) return a;
        long[][] temp = power_Matrix(a,n/2);
        if (n%2==0) return multi_Matrix(temp,temp);
        else return multi_Matrix(multi_Matrix(temp,temp),a);
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            long[][] matrix = new long[2][2];
            matrix[0][0]=0;
            matrix[0][1]=1;
            matrix[1][0]=1;
            matrix[1][1]=1;

            matrix = power_Matrix(matrix,n);
            System.out.println(matrix[0][1]);
        }
    }
}
