package contest2;

import java.util.Scanner;

public class B34 {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void sout(int count, int[] arr) {
        System.out.print("Buoc " + count + ":");
        for (int i : arr) {
            System.out.print( " " + i);
        }
        System.out.println();
    }

    static boolean ok(int[] arr){
        for (int i = 0 ; i < arr.length - 1; i++){
            if (arr[i] > arr[i+1]) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j+1]) {
                    swap(arr, j, j+1);
                }
            }
            sout(i+1,arr);
            if (ok(arr)) return;
        }

    }
}
