package contest2;

import java.util.Scanner;

public class B39 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            String[] arr = s.split(" ");
            for (int i = 0; i < arr.length; i++) {
                StringBuilder temp = new StringBuilder(arr[i]);
                arr[i] = temp.reverse().toString();
            }

            for (int i = 0; i < arr.length - 1; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println(arr[arr.length - 1]);
        }
    }
}
