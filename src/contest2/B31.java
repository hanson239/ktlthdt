package contest2;

import java.util.Scanner;

public class B31 {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void sout(int count, int[] arr) {
        System.out.print("Buoc " + count + ":");
        for (int i : arr) {
            System.out.print( " " + i);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        int count = 0;
        for (int i = 0; i < n-1; i++) {
            for (int j = i+1;j < n;j++) {
                if (arr[i] > arr[j]) {
                    swap(arr, i, j);
                }
            }

            sout(i+1,arr);
        }

    }
}

