package contest2;

import java.util.Scanner;

public class B36 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        long[] startIndexOfN = new long[51];
        for (int i = 1; i <= 50; i++) {
            startIndexOfN[i] = (long)Math.pow(2,i-1) - 1;
        }
        while (test-- > 0){
            int n = in.nextInt();
            long k = in.nextLong();
            k--;

            if (k % 2 == 0) {
                System.out.println(1);
                continue;
            }
            for (int i = n; i >=2 ;i--){
                if ( k == startIndexOfN[i]){
                    System.out.println(i);
                    break;
                }
                long temp = k - startIndexOfN[i];
                if (temp % Math.pow(2,i) == 0){
                    System.out.println(i);
                    break;
                }
            }
        }
    }
}
/*
1: 0 2 4 6 8    // 2
2: 1 5 9 13 17  // 4
3: 3 11         // 8
4: 7 23         // 16
5: 15 47        // 32




 */
