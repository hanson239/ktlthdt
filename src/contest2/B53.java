package contest2;

import java.util.Arrays;
import java.util.Scanner;

public class B53 {

    static char[] arr = new char[19];

    static void solve(int n) {
        while (true) {
            for (int i = 18; i >= 0; i--) {
                if (arr[i] == '0') {
                    arr[i] = '1';
                    for (int j = i + 1; j <= 18; j++) {
                        arr[j] = '0';
                    }
                    long temp = Long.parseLong(String.valueOf(arr));
                    if (temp % n == 0){
                        System.out.println(temp);
                        return ;
                    }
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0){
            int n = in.nextInt();
            Arrays.fill(arr,'0');
            solve(n);
        }
    }
}
