package contest2;

import java.util.Scanner;

public class B37 {

    static long[] f = new long[93];
    static void init(){
        f[0]=0;
        f[1] = 1;
        f[2] = 1;
        for (int i = 3; i<=92; i++){
            f[i] = f[i-1] + f[i-2];
        }
    }

    static String solve(long i,int n){
        if (i==1) {
            if (n % 2 == 0) return "B";
            else return "A";
        }

        if (i <= f[n-2]){
            return  solve(i,n-2);
        }
        else{
            return solve(i-f[n-2],n-1);
        }

    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        init();
        while (test-- > 0){
            int n = in.nextInt();
            long i = in.nextLong();
            System.out.println(solve(i,n));

        }
    }
}
/*
A
B
AB
BAB
ABBAB
BABABBAB


ABBABBABABBAB


BABABBABABBABBABABBAB



*/
