package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B43 {

    static Stack<Character> st = new Stack<>();

    static int solve(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (st.empty()) {
                st.push(s.charAt(i));
                continue;
            }
            char c = st.pop();
            if (!(c == '(' && s.charAt(i) == ')')) {
                st.push(c);
                st.push(s.charAt(i));
            }
        }
        int res = 0;
        while (!st.empty()) {
            char c = st.pop();
            char c2 = st.pop();
            if (c == '(' && c2 == '(') {
                res++;
                continue;
            }
            if (c == ')' && c2 == ')') {
                res++;
                continue;
            }
            if (c == '(' && c2 == ')') {
                res += 2;
                continue;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            System.out.println(solve(s));
        }
    }
}
