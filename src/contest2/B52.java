package contest2;

import java.util.Arrays;
import java.util.Scanner;

public class B52 {

    static char[] arr = new char[20];

    static int solve(String s) {
        int count = 0;
        while (true) {
            for (int i = 19; i >= 0; i--) {
                if (arr[i] == '0') {
                    arr[i] = '1';
                    for (int j = i + 1; j <= 19; j++) {
                        arr[j] = '0';
                    }
                    int offset = 0;
                    while (arr[offset] != '1') offset++;
                    String ss = String.valueOf(arr, offset, 20 - offset);
                    boolean temp = isSmaller(ss, s);
                    if (isSmaller(ss, s)) {
                        count++;
                        break;
                    } else return count;
                }
            }
        }
    }

    static boolean isSmaller(String s, String ss) {
        if (s.length() < ss.length()) return true;
        if (s.length() > ss.length()) return false;
        if (s.compareTo(ss) <= 0) return true;
        return false;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            Arrays.fill(arr,'0');
            System.out.println(solve(s));
        }
    }
}
