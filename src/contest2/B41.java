package contest2;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class B41 {

    static Stack<Character> st = new Stack();
    static Stack<Integer> index = new Stack<>();
    static int ok(String input, int[] mark) {
        int max = Integer.MIN_VALUE;
        st.clear();
        index.clear();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (st.isEmpty()) {
                st.push(c);
                index.push(i);
            }
            else {
                char temp = st.peek();
                if (temp == '(' && c == ')') {
                    st.pop();
                    int Index = index.pop();
                    mark[i] = 1;
                    mark[Index] = 1;
                    continue;
                }
                st.push(c);
                index.push(i);
            }
        }

        for (int i = 1; i <input.length(); i++) {
            if (mark[i] == 1){
                mark[i] = mark[i-1] + 1;
            }
            if (max < mark[i]){
                max = mark[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String input = in.nextLine();
            int[] mark = new int[input.length()];
            Arrays.fill(mark, 0);
            System.out.println(ok(input, mark));
        }
    }
}
