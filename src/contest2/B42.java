package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B42 {

    static Stack<Character> stChar = new Stack<>();
    static Stack<Integer> stIndex = new Stack<>();

    static boolean check(String input){
        for (int i = 0; i< input.length();i++){
            if (input.charAt(i) == '('){
                stChar.add(input.charAt(i));
                stIndex.add(i);
                continue;
            }
            if ( input.charAt(i) == ')'){
                if (stChar.empty()){
                    stChar.add(input.charAt(i));
                    stIndex.add(i);
                    continue;
                }

                char c = stChar.pop();
                int index = stIndex.pop();

                if (c =='(' && input.charAt(i) == ')'){
                    if (index + 4 > i) return false;
                    if (!stChar.empty() && stChar.peek() == '(') {
                        int indexright = i;
                        i++;

                        while (i < input.length() && input.charAt(i) != ')') {
                            i++;
                        }
                        char cleft = stChar.pop();
                        int indexleft = stIndex.pop();
                        if (cleft == '(' && input.charAt(i) == ')') {
                            if (index - indexleft == 1) {
                                if (i - indexright == 1) {
                                    return false;
                                } else {
                                    stChar.push(cleft);
                                    stChar.push(c);
                                    stChar.push(input.charAt(indexright));
                                    stChar.push(input.charAt(i));
                                }
                            }
                        }
                    }
                }

            }

        }
        return true;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String input = in.nextLine();
            input = input.replaceAll("\\s+","");

            stChar.clear();
            stIndex.clear();
            if(!check(input)){
                System.out.println("Yes");
            }
            else{
                System.out.println("No");
            }
        }
    }
}
