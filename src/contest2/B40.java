package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B40 {

    static String ok(String input){
        Stack<Character> st = new Stack();
        for (int i = 0; i<input.length();i++){
            char c = input.charAt(i);
            if(st.isEmpty()) st.push(c);
            else {
                char temp = st.peek();
                if (temp == '[' && c == ']') {st.pop(); continue;}
                if (temp == '(' && c == ')') {st.pop(); continue;}
                if (temp == '{' && c == '}') {st.pop(); continue;}
                st.push(c);
            }
        }
        if (st.isEmpty()) return "YES";
        return "NO";
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0){
            String input = in.nextLine();
            System.out.println(ok(input));
        }
    }
}
