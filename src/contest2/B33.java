package contest2;

import java.util.*;

public class B33 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        List<Integer> q = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            q.add(in.nextInt());
            Collections.sort(q);
            System.out.print("Buoc " + i + ":");

            for (int temp : q){
                System.out.print( " " + temp);
            }
            System.out.println();
        }



    }
}
