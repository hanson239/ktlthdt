package contest2;

import java.util.Scanner;

public class B56 {

    static long solve(int s, int t) {
        if (t <= s) return s - t;
        if (t % 2 == 0) return 1 + solve(s, t / 2);
        return 2 + solve(s, (t + 1) / 2);
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int S = in.nextInt();
            int T = in.nextInt();

            System.out.println(solve(S,T));

        }
    }
}
