package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B47 {

    static boolean isOperator(char x) {
        switch (x) {
            case '+':
            case '-':
            case '/':
            case '*':
                return true;
        }
        return false;
    }

    static int valueOfPostfix(String s) {
        Stack<Integer> st = new Stack<>();
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            if (isOperator(c)) {
                int a = st.pop();
                int b = st.pop();
                if (c == '+') {
                    st.push(a + b);
                } else if (c == '-') {
                    st.push(a - b);
                } else if (c == '*') {
                    st.push(a * b);
                } else if (c == '/') {
                    st.push(a / b);
                }
            } else {
                st.push(c-'0');
            }
        }
        return st.pop();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String s = in.nextLine();
            System.out.println(valueOfPostfix(s));
        }
    }
}
