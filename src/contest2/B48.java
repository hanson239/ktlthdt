package contest2;//import java.util.ArrayList;
//import java.util.Scanner;
//import java.util.Stack;
//
//public class contest2.B48 {
//
//    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int test = in.nextInt();
//        while (test-- > 0) {
//            int n = in.nextInt();
//            ArrayList<Long> arr = new ArrayList<>();
//            ArrayList<Integer> ok = new ArrayList<>();
//            Stack<Integer> st = new Stack<>();
//
//            for (int i = 0; i < n; i++) {
//                arr.add(in.nextLong());
//                ok.add(-1);
//            }
//
//            st.push(0);
//            for (int i = 1; i < n; i++) {
//                while (!st.empty() && (arr.get(i) > arr.get(st.peek()))) {
//                    ok.set(st.pop(), i);
//                    if (st.empty()) break;
//                }
//                st.push(i);
//            }
//            for (int i = 0; i < n; i++) {
//                if (ok.get(i) < 0) System.out.print("-1 ");
//                else System.out.print(arr.get(ok.get(i)) + " ");
//            }
//            System.out.println();
//        }
//    }
//}

import java.util.Scanner;
import java.util.Stack;

public class B48 {
    public static void main(String[] args) {

        int n, t;
        Scanner in = new Scanner(System.in);
        t = in.nextInt();
        while (t-- > 0) {
            n = in.nextInt();
            long[] k = new long[n];
            Stack<Long> st = new Stack<Long>();
            for (int i = 0; i < n; i++) {
                k[i] = in.nextLong();
            }
            st.push(k[0]);

            for (int i = 1; i < n; i++) {
                while (!st.empty() && st.peek() < k[i]) {
                    System.out.print(k[i] + " ");
                    st.pop();
                }
                st.push(k[i]);

            }

            while (!st.empty()) {
                System.out.print("-1" + " ");
                st.pop();
            }
            System.out.println("");
        }

    }
}
/*
100
5
5 3 4 2 3
 */
