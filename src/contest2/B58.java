package contest2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class B58 {

    static int[][] arr;
    static int[][] ok;

    static void BFS(int n, int m) {
        Queue<Integer> qx = new LinkedList<>();
        Queue<Integer> qy = new LinkedList<>();
        qx.add(1);
        qy.add(1);
        ok[1][1] = 0;
        while (!qx.isEmpty()) {
            int x = qx.poll();
            int y = qy.poll();
            if (y + arr[x][y] <= m)
                if (ok[x][y + arr[x][y]] == -1) {
                    ok[x][y + arr[x][y]] = ok[x][y] + 1;
                    qx.add(x);
                    qy.add(y + arr[x][y]);
                }
            if (x + arr[x][y] <= n)
                if (ok[x + arr[x][y]][y] == -1) {
                    ok[x + arr[x][y]][y] = ok[x][y] + 1;
                    qx.add(x + arr[x][y]);
                    qy.add(y);
                }
            if (ok[n][m] != -1) {
                return;
            }
        }
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int m = in.nextInt();
            arr = new int[n + 1][m + 1];
            ok = new int[n + 1][m + 1];

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    arr[i][j] = in.nextInt();
                    ok[i][j] = -1;
                }
            }
            BFS(n, m);
            System.out.println(ok[n][m]);

        }
    }
}
