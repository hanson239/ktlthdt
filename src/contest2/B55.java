package contest2;

import java.util.Scanner;

public class B55 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int k = in.nextInt();
            String s = in.next();
            if (k >= s.length()) {
                System.out.println(0);
                continue;
            }
            int[] count = new int[26];
            for (int i = 0; i < s.length(); i++) {
                count[s.charAt(i) - 'A']++;
            }
            while (k-- > 0) {
                int max = count[0];
                int offset = 0;
                for (int i = 1; i < 26; i++) {
                    if (count[i] > max) {
                        max = count[i];
                        offset = i;
                    }
                }
                count[offset]--;
            }
            long res = 0;
            for (int i : count) {
                res += Math.pow(i,2);
            }
            System.out.println(res);
        }
    }
}
