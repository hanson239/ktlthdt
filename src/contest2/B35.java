package contest2;

import java.util.Scanner;

public class B35 {

    static final long module = 1000000007;

    static long power(long n, long k) {
        if (k == 0) return 1;
        if (k == 1) return n;
        long temp = (power(n, k / 2) % module);
        if (k % 2 == 0) return (temp * temp) % module;
        return (((temp * temp) % module) * n) % module;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long test = in.nextInt();
        while (test-- > 0) {
            long n = in.nextInt();
            long k = in.nextInt();
            System.out.println(power(n,k));
        }
    }
}
