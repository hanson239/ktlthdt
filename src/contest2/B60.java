package contest2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class B60 {

    static int n;
    static char[][] arr;
    static boolean[][] checked;
    static int[] positionStart = new int[2];
    static int[] positionEnd = new int[2];
    static Queue<int[]> qxy = new LinkedList<>();
    static Queue<Integer> qRes = new LinkedList<>();

    static void solve(int x, int y, int res) {
        if (checked[x][y] == false) {
            checked[x][y] = true;
            int[] temp = {x, y};
            qxy.add(temp);
            qRes.add(res + 1);
        }
    }

    static int BFS() {
        qxy.clear();
        qRes.clear();
        qxy.add(positionStart);
        qRes.add(0);
        while (!qRes.isEmpty()) {
            int[] xy = qxy.poll();
            int res = qRes.poll();
            if (xy[0] == positionEnd[0] && xy[1] == positionEnd[1]) {
                return res;
            }

            for (int i = xy[0] - 1; i >= 0; i--) {
                if (arr[i][xy[1]] == 'X') break;
                solve(i, xy[1], res);
            }

            for (int i = xy[0] + 1; i < n; i++) {
                if (arr[i][xy[1]] == 'X') break;
                solve(i, xy[1], res);
            }

            for (int i = xy[1] - 1; i >= 0; i--) {
                if (arr[xy[0]][i] == 'X') break;
                solve(xy[0], i, res);
            }

            for (int i = xy[1] + 1; i < n; i++) {
                if (arr[xy[0]][i] == 'X') break;
                solve(xy[0], i, res);
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            arr = new char[n][n];
            checked = new boolean[n][n];

            for (int i = 0; i < n; i++) {
                String s = in.next();
                for (int j = 0; j < n; j++) {
                    arr[i][j] = s.charAt(j);
                    checked[i][j] = false;
                }
            }
            positionStart[0] = in.nextInt();
            positionStart[1] = in.nextInt();
            positionEnd[0] = in.nextInt();
            positionEnd[1] = in.nextInt();

            System.out.println(BFS());
        }
    }
}
