package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B49 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            long res = 0;
            long[] a = new long[n];
            Stack<Integer> b = new Stack<>();
            for (int i = 0; i < n; i++) {
                a[i] = in.nextLong();
            }
            for (int i = 0; i < n; i++) {
                if (b.empty()) {
                    b.push(i);
                    continue;
                }
                if (a[b.peek()] <= a[i]) {
                    b.push(i);
                    continue;
                }
                int temp = b.pop();
                if (b.empty()) {
                    res = Math.max(res, a[temp] * i);
                } else
                    res = Math.max(res, a[temp] * (i - b.peek() - 1));
                i--;
            }
            while (!b.empty()) {
                int temp = b.pop();
                if (b.empty()) {
                    res = Math.max(res, a[temp] * n);
                } else
                    res = Math.max(res, a[temp] * (n - b.peek() - 1));
            }
            System.out.println(res);
        }
    }
}
