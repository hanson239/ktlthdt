package contest2;

import java.util.Scanner;
import java.util.Stack;

public class B50 {

    static void Solve(String s) {
        Stack<Integer> st = new Stack<>();
        if (s.charAt(s.length() - 1) == 'I') s += "I";
        else s += "D";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'I' || i == s.length() - 1) {
                System.out.print(i + 1);
                while (!st.empty()) {
                    System.out.print(st.pop());
                }
            } else if (s.charAt(i) == 'D') st.push(i + 1);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            String s = in.next();
            Solve(s);
        }
    }
}
